﻿namespace DixiOmnes.PreflopPrism.Web.Models.Http
{
    public class RatingViewModel
    {
        public int ProfileID { get; set; }
        public double AverageRating { get; set; } 
        public int UserRating { get; set; }
        public int VoteTotal { get; set; }
    }
}
