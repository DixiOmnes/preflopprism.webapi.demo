﻿using DixiOmnes.PreflopPrism.Web.Models.Common;

namespace DixiOmnes.PreflopPrism.Web.Models.Http
{
    public class ProfileViewModel
    {
        public int? ID { get; set; }
        public string Name { get; set; }
        public string GameType { get; set; }
        public int StackSize { get; set; }
        public int TableMax { get; set; }
        public string Description { get; set; }        
        public ProfileStatuses Status { get; set; }        
        public string ActionLayers { get; set; }
        public RatingViewModel Rating { get; set; }
        public string Version { get; set; }
        public string Owner { get; set; }
    }
}
