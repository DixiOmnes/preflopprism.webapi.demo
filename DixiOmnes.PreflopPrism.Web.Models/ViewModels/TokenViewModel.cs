﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DixiOmnes.PreflopPrism.Web.Models.Exchange
{
    public class TokenViewModel
    {
        public string access_token { get; set; }
        public int lifetime { get; set; }
    }
}
