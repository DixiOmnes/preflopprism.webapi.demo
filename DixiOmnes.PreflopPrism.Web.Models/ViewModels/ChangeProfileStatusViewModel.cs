﻿using DixiOmnes.PreflopPrism.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Models.Http
{
    public class ChangeProfileStatusViewModel
    {
        public int ID { get; set; }        
        public ProfileStatuses Status { get; set; }
    }
}