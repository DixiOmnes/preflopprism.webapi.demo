﻿using DixiOmnes.PreflopPrism.Web.Models.Common;
using System;
using System.Net.Mail;

namespace DixiOmnes.PreflopPrism.Web.Models.Http
{
    public class UserViewModel
    {        
        public string UserName { get; set; }
        public string EMail { get; set; }
        public UserStatuses Status { get; set; }        
    }
}
