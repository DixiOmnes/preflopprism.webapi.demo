﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Models.Entities
{
    [Table("hhashes")]
    public class HardwareHash
    {
        [Key]
        [Column("hhash_id")]
        public virtual int Id { get; set; }

        [Required]
        [Column("hhash_value")]
        [MaxLength(50)]
        public virtual string Value { get; set; }

        [Column("user_id")]
        public virtual int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
