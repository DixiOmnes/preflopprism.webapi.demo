﻿using DixiOmnes.PreflopPrism.Web.Models.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DixiOmnes.PreflopPrism.Web.Models.Entities
{
    [Table("profiles")]
    public class Profile
    {
        [Key]
        [Column("profile_id")]
        public virtual int ID { get; set; }

        [Required]
        [Column("name")]
        [MaxLength(80)]
        public virtual string Name { get; set; }

        [Required]
        [Column("game_type")]
        [MaxLength(10)]
        public virtual string GameType { get; set; }

        [Required]
        [Column("stack_size")]        
        public virtual int StackSize { get; set; }

        [Required]
        [Column("table_max")]
        public virtual int TableMax { get; set; }        

        [Column("description")]
        [MaxLength(1000)]
        public virtual string Description { get; set; }

        [Column("version")]
        [MaxLength(20)]
        public virtual string Version { get; set; }

        [Required]
        [Column("status")]
        [EnumDataType(enumType: typeof(ProfileStatuses))]
        public virtual ProfileStatuses Status { get; set; }

        [Required]
        [Column("action_layers")]        
        public virtual string ActionLayers { get; set; }
        
        [Column("average_rating")]
        public virtual double AverageRating { get; set; }

        [Column("user_id")]
        public virtual int UserId { get; set; }
        public virtual User User { get; set; }

    }
}
