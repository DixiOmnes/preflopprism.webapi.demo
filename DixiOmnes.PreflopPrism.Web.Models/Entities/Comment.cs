﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Models.Entities
{
    [Table("comments")]
    public class Comment
    {
        [Column("comment_id")]
        public virtual int ID { get; set; }

        [Column("profile_id")]
        public virtual int ProfileID { get; set; }
        public virtual Profile Profile { get; set; }
        
        [Column("user_id")]
        public virtual int UserID { get; set; }
        public virtual User User { get; set; }

        [Column("date")]
        public virtual DateTime DateTime { get; set; }

        [Column("comment")]
        [MaxLength(1000)]
        public virtual string Body { get; set; }

        [Column("like_count")]
        public virtual int LikeCount { get; set; }

        [Column("dislike_count")]
        public virtual int DisLikeCount { get; set; }
    }

    
}
