﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DixiOmnes.PreflopPrism.Web.Models.Entities
{
    [Table("logs")]
    public class LogRecord
    {        
        public enum CommandResult { Success = 1, Exception = 2, Rejected = 3, ValidationError = 4 };

        [Key]
        [Column("id")]
        public virtual int Id { get; set; }

        [Column("datetime")]
        public virtual DateTime DateTime { get; set; }

        [Column("user_id")]
        public virtual int UserId { get; set; }
        public virtual User User { get; set; }

        [Column("command")]
        public virtual string Command { get; set; }

        [Column("command_result")]
        public virtual CommandResult Result { get; set; }

        [Column("notes")]
        [MaxLength(255)]
        public virtual string Notes { get; set; }
    }
}
