﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Models.Entities
{
    [Table("ratings")]
    public class Rating
    {
        
        [Column("user_id")]
        public virtual int UserID { get; set; }        
        public virtual User User { get; set; }

        [Column("profile_id")]
        public virtual int ProfileID { get; set; }        
        public virtual Profile Profile { get; set; }        

        [Required]
        [Column("rating_value")]        
        public virtual int Value { get; set; }        

        [Column("date")]        
        public virtual DateTime DateTime { get; set; }        

    }
}
