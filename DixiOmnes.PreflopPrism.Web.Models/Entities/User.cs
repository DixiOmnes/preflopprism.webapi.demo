﻿using DixiOmnes.PreflopPrism.Web.Models.Common;
using DixiOmnes.PreflopPrism.Web.Repositories;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DixiOmnes.PreflopPrism.Web.Models.Entities
{
    [Table("users")]
    public class User : BaseEntity
    {
        [Key]
        [Column("user_id")]
        public virtual int Id { get; set; }

        [Column("name")]
        [MaxLength(50)]
        public virtual string Name { get; set; }        

        [Column("e_mail")]
        [MaxLength(100)]
        public virtual string EMail { get; set; }

        [Column("status")]
        [EnumDataType(enumType: typeof(UserStatuses))]
        public virtual UserStatuses Status { get; set; }
    }
}
