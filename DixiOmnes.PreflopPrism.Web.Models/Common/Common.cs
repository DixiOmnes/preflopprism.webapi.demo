﻿namespace DixiOmnes.PreflopPrism.Web.Models.Common
{
    public enum ProfileStatuses { Unknown = 0, Published = 1, Private = 2, Hidden = 3 }
    public enum UserStatuses { Unknown = 0, NotRegistered = 1, EmailValidation = 2, Registered = 3 }
}