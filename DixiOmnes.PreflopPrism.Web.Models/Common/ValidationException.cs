﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Models.Common
{
    public class PPValidationException : Exception
    {
        private string _loggerMsg;
        public string LoggerMsg
        {
            get { return _loggerMsg; }
            private set { _loggerMsg = value; }
        }

        public PPValidationException(string messageForUser, string messageForLogger)
            :base(messageForUser)
        {

            LoggerMsg = messageForLogger ?? messageForUser;
        }

        public PPValidationException(string messageForUser)
            : base(messageForUser)
        {

            LoggerMsg = messageForUser;
        }
    }


    public static class ExceptionExtensions
    {
        public static IEnumerable<Exception> GetInnerExceptions(this Exception ex)
        {
            if (ex == null)
            {
                throw new ArgumentNullException("ex");
            }

            var innerException = ex;
            do
            {
                yield return innerException;
                innerException = innerException.InnerException;
            }
            while (innerException != null);
        }
    }
}
