﻿using FluentValidation;

namespace DixiOmnes.PreflopPrism.Web.Models.Http
{
    public class UserViewModelValidator : AbstractValidator<UserViewModel>
    {
        public UserViewModelValidator()
        {
            RuleFor(model => model.EMail)
                .NotEmpty().WithMessage("Поле \"{PropertyName}\" не заполнено.")
                .MaximumLength(100).WithMessage("Поле \"{PropertyName}\" не должно быть длиннее 100 символов.")
                .EmailAddress().WithMessage("Поле \"{PropertyName}\" не является E-mail адресом.")
                .WithName("E-mail");

            RuleFor(model => model.UserName)
                .NotEmpty().WithMessage("Поле \"{PropertyName}\" не заполнено.")
                .MaximumLength(50).WithMessage("Поле \"{PropertyName}\" не должно быть длиннее 50 символов.")
                .WithName("Username");            
        }
    }
}
