﻿using FluentValidation;

namespace DixiOmnes.PreflopPrism.Web.Models.Http
{
    public class ProfileViewModelValidator : AbstractValidator<ProfileViewModel>
    {
        public ProfileViewModelValidator()
        {
            RuleFor(model => model.Name)
                    .NotEmpty().WithMessage("Поле \"{PropertyName}\" не заполнено.")
                    .MaximumLength(80).WithMessage("Поле \"{PropertyName}\" не должно содержить больше 80 символов")
                    .WithName("Name");

            RuleFor(model => model.GameType)
                .NotEmpty().WithMessage("Поле \"{PropertyName}\" не заполнено.")
                .MaximumLength(10).WithMessage("Поле \"{PropertyName}\" не должно содержить больше 10 символов")
                .WithName("GameType");

            RuleFor(model => model.Description)
                .MaximumLength(1000).WithMessage("Поле \"{PropertyName}\" не должно содержить больше 1000 символов")
                .WithName("Description");

            RuleFor(model => model.Status)
                .IsInEnum().WithMessage("Поле \"{PropertyName}\" содержит некорректное значение")
                .WithName("Status");

            RuleFor(model => model.Version)
                .MaximumLength(20).WithMessage("Поле \"{PropertyName}\" не должно содержить больше 20 символов")
                .WithName("Version");

            RuleFor(model => model.StackSize)
                    .Must(x => x > 0).WithMessage("Поле \"{PropertyName}\" не может быть равно 0")
                    .WithName("StackSize");

            RuleFor(model => model.TableMax)
                .Must(x => x > 0).WithMessage("Поле \"{PropertyName}\" не может быть равно 0")
                .WithName("TableMax");
        }
    }
}
