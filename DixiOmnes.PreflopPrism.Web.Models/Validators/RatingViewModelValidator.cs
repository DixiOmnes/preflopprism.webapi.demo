﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Models.Http
{
    public class RatingViewModelValidator : AbstractValidator<RatingViewModel>
    {
        public RatingViewModelValidator()
        {
            RuleFor(model => model.ProfileID)
                    .Must(x => x > 0).WithMessage("Поле \"{PropertyName}\" должно быть > 0")
                    .WithName("ProfileID");

            RuleFor(model => model.UserRating)
                    .Must(x => x >= 0 && x <= 5).WithMessage("Поле \"{PropertyName}\" должно находиться в пределах 0..5")
                    .WithName("UserRating");
        }
    }
}