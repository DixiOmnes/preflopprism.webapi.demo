﻿using FluentValidation;

namespace DixiOmnes.PreflopPrism.Web.Models.Http
{
    public class ChangeProfileStatusViewModelValidator : AbstractValidator<ChangeProfileStatusViewModel>
    {
        public ChangeProfileStatusViewModelValidator()
        {
            RuleFor(model => model.Status)
                .IsInEnum().WithMessage("Поле \"{PropertyName}\" содержит некорректное значение")
                .WithName("Status");

            RuleFor(model => model.ID)
                        .Must(x => x > 0).WithMessage("Поле \"{PropertyName}\" должно быть положительным ненулевым")
                        .WithName("ID");
        }
    }
}
