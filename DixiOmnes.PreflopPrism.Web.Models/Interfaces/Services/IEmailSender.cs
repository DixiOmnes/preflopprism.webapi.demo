﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services
{
    public interface IEmailSender
    {
        void Send(string userName, string password, string host, MailMessage msg);
    }
}
