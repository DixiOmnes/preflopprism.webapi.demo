﻿using DixiOmnes.PreflopPrism.Web.Models.Entities;
using DixiOmnes.PreflopPrism.Web.Models.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services
{
    public interface IProfileService
    {
        Task ChangeProfileStatusAsync(User user, ChangeProfileStatusViewModel requestProfile);
        Task<int> UpdateProfileAsync(User user, ProfileViewModel httpContextProfile);
        Task<ProfileViewModel> GetProfileAsync(User user, int profileID);
        Task<IEnumerable<ProfileViewModel>> GetProfilesListAsync(User user);
    }
}
