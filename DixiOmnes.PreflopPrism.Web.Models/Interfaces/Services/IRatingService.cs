﻿using DixiOmnes.PreflopPrism.Web.Models.Entities;
using DixiOmnes.PreflopPrism.Web.Models.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services
{
    public interface IRatingService
    {
        Task<RatingViewModel> ChangeProfileRating(User user, RatingViewModel httpRating);
    }
}
