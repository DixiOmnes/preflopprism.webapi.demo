﻿using DixiOmnes.PreflopPrism.Web.Models.Entities;
using DixiOmnes.PreflopPrism.Web.Models.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services
{
    public interface IUserService
    {
        Task<UserViewModel> SendEmailVerificationLetterAsync(User userJWT, UserViewModel userHTTPContext);
        Task<UserViewModel> ValidateEmailAsync(User userJWT, string emailCodeFromHttpContext);
        Task<UserViewModel> CancelUserStatusAsync(User userJWT);
        Task<UserViewModel> GetUserAsync(User userJWT);
    }
}
