﻿using DixiOmnes.PreflopPrism.Web.Models.Entities;
using DixiOmnes.PreflopPrism.Web.Models.Exchange;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services
{
    public interface IAccountService
    {
        Task<TokenViewModel> CreateTokenFromHHashAsync(string hhash);
        TokenViewModel CreateToken(User dbUser);        
    }
}
