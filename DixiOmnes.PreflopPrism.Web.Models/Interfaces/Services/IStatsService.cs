﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services
{
    public interface IStatsService
    {
        Task<string> GetStatsInHTMLForm(int code, int hours, string webRootPath);
    }
}
