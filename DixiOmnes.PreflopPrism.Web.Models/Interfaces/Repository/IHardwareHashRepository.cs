﻿using DixiOmnes.PreflopPrism.Web.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Repositories
{
    public interface IHardwareHashRepository : IRepository
    {
        Task<HardwareHash> GetHardwareHashAsync(string hhashString);
        Task InserthardwareHashAsync(HardwareHash hhash);
    }
}
