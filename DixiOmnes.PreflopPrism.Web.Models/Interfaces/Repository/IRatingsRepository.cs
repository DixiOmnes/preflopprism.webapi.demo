﻿using DixiOmnes.PreflopPrism.Web.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Repositories
{
    public interface IRatingsRepository : IRepository
    {
        Task<Rating> GetRatingAsync(int profileID, int userID);
        void RemoveRating(Rating rating);
        Task InsertRatingAsync(Rating rating);
        Task<int> GetTotalVotesForProfile(int profileID);
        Task<double> GetAverageVotesForProfile(int profileID);
    }
}
