﻿using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Repositories
{
    public interface IRepository
    {
        Task SaveChangesAsync();        
    }
}