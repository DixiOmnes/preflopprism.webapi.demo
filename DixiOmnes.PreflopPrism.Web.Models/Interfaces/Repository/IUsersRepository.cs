﻿using DixiOmnes.PreflopPrism.Web.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Repositories
{
    public interface IUsersRepository : IRepository
    {
        Task<User> GetUserAsync(int userID);

        Task InsertUserAsync(User user);

        Task<bool> EmailExistsAsync(string email);

        Task<bool> UserNameExistsAsync(string name);
    }
}
