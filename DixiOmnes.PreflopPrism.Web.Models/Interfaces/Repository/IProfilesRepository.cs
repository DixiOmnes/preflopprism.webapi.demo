﻿using DixiOmnes.PreflopPrism.Web.Models.Entities;
using DixiOmnes.PreflopPrism.Web.Models.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Repositories
{
    public interface IProfilesRepository : IRepository
    {
        Task<Profile> GetProfileAsync(int profileID);
        Task InsertProfileAsync(Profile profile);
        Task<IEnumerable<Profile>> GetPrivateProfilesForUser(int userID);
        Task<IEnumerable<ProfileViewModel>> GetAllProfilesListForUser(int userID);
    }
}
