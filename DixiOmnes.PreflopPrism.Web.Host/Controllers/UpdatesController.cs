﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DixiOmnes.PreflopPrism.Web.Filters;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;


namespace DixiOmnes.PreflopPrism.Web.Controllers
{
    [Route("api/[controller]")]
    public class UpdatesController : Controller
    {
        private IHostingEnvironment _environment;

        public UpdatesController(IHostingEnvironment env)
        {
            _environment = env;
        }

        [HttpPost]
        [Route("get_recent_update")]
        public async Task<IActionResult> GetRecentUpdate()
        {
            var webRoot = _environment.WebRootPath;
            var file = System.IO.Path.Combine(webRoot, @"updates\updates.txt");
            var filecontent = await System.IO.File.ReadAllTextAsync(file);

            return Ok(new {
            ExcecutedCommands = "updates/get_recent_update",
            ResultCode = 200,
            Result = filecontent
            });
            //return Ok(filecontent);
        }

        //исправлен косяк на скорую руку. придумать норм решение
        [HttpPost]
        [Route("get_recent_update27")]
        public async Task<IActionResult> GetRecentUpdate27()
        {
            var webRoot = _environment.WebRootPath;
            var file = System.IO.Path.Combine(webRoot, @"updates\updates.txt");
            var filecontent = await System.IO.File.ReadAllTextAsync(file);

            return Ok(filecontent);
        }
    }
}
