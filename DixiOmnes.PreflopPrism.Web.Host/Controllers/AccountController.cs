﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using DixiOmnes.PreflopPrism.Web.Models.Entities;
using Microsoft.AspNetCore.Http;
using DixiOmnes.PreflopPrism.Web.Utils;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using DixiOmnes.PreflopPrism.Web.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using DixiOmnes.PreflopPrism.Web.Repositories;
using System.Transactions;
using DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DixiOmnes.PreflopPrism.Web.Controllers
{    
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        IAccountService _accountService;        

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;            
        }

        [HttpPost]
        [Route("token")]
        public async Task<IActionResult> Token([FromBody]string hhash)
        {
            return Ok(await _accountService.CreateTokenFromHHashAsync(hhash));
        }        
    }
}
