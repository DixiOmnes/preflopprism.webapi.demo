﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DixiOmnes.PreflopPrism.Web.Models.Entities;
using DixiOmnes.PreflopPrism.Web.Utils;
using DixiOmnes.PreflopPrism.Web.Filters;
using Microsoft.AspNetCore.Authorization;
using DixiOmnes.PreflopPrism.Web.Models.Common;
using DixiOmnes.PreflopPrism.Web.Models.Http;
using DixiOmnes.PreflopPrism.Web.Repositories;
using System.Transactions;
using DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services;

namespace DixiOmnes.PreflopPrism.Web.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Registered")]
    public class RatingsController : PPBaseController
    {
        IRatingService _ratingService;

        public RatingsController(IRatingService ratingService)
        {
            _ratingService = ratingService;
        }

        [HttpPost]
        [Route("change_profile_rating")]
        public async Task<IActionResult> ChangeProfileRating([FromBody]RatingViewModel httpRating)
        {
            var rating = await _ratingService.ChangeProfileRating(TokenUser, httpRating);

            return Ok(rating);
        }
    }
}
