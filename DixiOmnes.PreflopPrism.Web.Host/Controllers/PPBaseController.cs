﻿using DixiOmnes.PreflopPrism.Web.Models.Entities;
using DixiOmnes.PreflopPrism.Web.Repositories;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Controllers
{
    public class PPBaseController : Controller
    {
        User _tokenUser;

        public User TokenUser
        {
            get
            {
                if (_tokenUser == null)
                    _tokenUser = JsonConvert.DeserializeObject<User>(User.FindFirst("User").Value);

                return _tokenUser;
            }
        }

        public PPBaseController()
        {
            
        }

        public async Task WriteSuccessLogAsync(string notes = "")
        {
            //TODO: Добавить репозиторий Логгер и писать через него. Либо разобраться с ILogger
            //await _repository.WriteSuccessLogAsync(Request.Path.Value, TokenUser.Id, notes);
        }
    }
}
