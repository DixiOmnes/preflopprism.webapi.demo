﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DixiOmnes.PreflopPrism.Web.Filters;
using DixiOmnes.PreflopPrism.Web.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using DixiOmnes.PreflopPrism.Web.Models.Common;
using DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services;

namespace PPWebAPI.Controllers
{
    [Route("api/[controller]")]
    public class StatsController : Controller
    {        
        IHostingEnvironment _environment;
        IStatsService _statsService;

        public StatsController(IStatsService statsService, IHostingEnvironment env)
        {
            _statsService = statsService;
            _environment = env;
        }        

        [HttpGet]        
        public async Task<IActionResult> GetStats(int code, int hours)
        {
            var htmpPage = await _statsService.GetStatsInHTMLForm(code, hours, _environment.WebRootPath);

            return new ContentResult()
            {
                Content = htmpPage,
                ContentType = "text/html",
            };
               
        }
    }
}