﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DixiOmnes.PreflopPrism.Web.Models.Entities;
using Newtonsoft.Json;
using System.Net.Mail;
using DixiOmnes.PreflopPrism.Web.Filters;
using DixiOmnes.PreflopPrism.Web.Utils;
using Microsoft.AspNetCore.Authorization;
using DixiOmnes.PreflopPrism.Web.Models.Common;
using DixiOmnes.PreflopPrism.Web.Models.Http;
using System.Text;
using DixiOmnes.PreflopPrism.Web.Repositories;
using DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DixiOmnes.PreflopPrism.Web.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class UsersController : PPBaseController
    {
        IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [Route("get_user_status")]
        public async Task<IActionResult> GetUserStatus([FromBody]string param)
        {
            var user = await _userService.GetUserAsync(TokenUser);

            return Ok(user);
        }
                
        [HttpPost]
        [Route("register_user")]
        [Authorize(Roles = "NotRegistered")]
        public async Task<IActionResult> RegisterUser([FromBody]UserViewModel requestUser)
        {
            var user = await _userService.SendEmailVerificationLetterAsync(TokenUser, requestUser);

            return Ok(user);
        }

        [HttpPost]
        [Route("validate_email")]
        [Authorize(Roles = "EmailValidation")]
        public async Task<IActionResult> ValidateEmail([FromBody]string emailCodeFromHttpContext)
        {
            var user = await _userService.ValidateEmailAsync(TokenUser, emailCodeFromHttpContext);

            return Ok(user);
        }

        [HttpPost]
        [Route("cancel_user_status")]
        public async Task<IActionResult> CancelUserStatus()
        {
            var user = await _userService.CancelUserStatusAsync(TokenUser);

            return Ok(user);
        }        
    }
}
