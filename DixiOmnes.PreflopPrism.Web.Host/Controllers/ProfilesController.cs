﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DixiOmnes.PreflopPrism.Web.Filters;
using DixiOmnes.PreflopPrism.Web.Models.Entities;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using DixiOmnes.PreflopPrism.Web.Models.Common;
using DixiOmnes.PreflopPrism.Web.Models.Http;
using DixiOmnes.PreflopPrism.Web.Repositories;
using DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DixiOmnes.PreflopPrism.Web.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Registered")]
    public class ProfilesController : PPBaseController
    {
        IProfileService _profileService;

        public ProfilesController(IProfileService profileService)
        {
            _profileService = profileService;
        }

        [HttpPost]
        [Route("change_profile_status")]
        public async Task<IActionResult> ChangeProfileStatus([FromBody]ChangeProfileStatusViewModel requestProfile)
        {
            await _profileService.ChangeProfileStatusAsync(TokenUser, requestProfile);

            return Ok();
        }

        [HttpPost]
        [Route("update_profile")]
        public async Task<IActionResult> UpdateProfile([FromBody]ProfileViewModel httpContextProfile)
        {
            var profileID = await _profileService.UpdateProfileAsync(TokenUser, httpContextProfile);

            return Ok(profileID);
        }

        [HttpPost]
        [Route("get_profile")]
        public async Task<IActionResult> GetProfile([FromBody]int profileId)
        {
            var httpProfile = await _profileService.GetProfileAsync(TokenUser, profileId);

            return Ok(httpProfile);
        }

        [HttpPost]
        [Route("get_profile_list")]
        public async Task<IActionResult> GetProfileList()
        {
            var httpProfiles = await _profileService.GetProfilesListAsync(TokenUser);

            return Ok(httpProfiles);
        }
    }
}
