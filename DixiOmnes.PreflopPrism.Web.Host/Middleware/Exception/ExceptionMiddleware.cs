﻿using DixiOmnes.PreflopPrism.Web.Models.Common;
using DixiOmnes.PreflopPrism.Web.Models.Common;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next)); 
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception exception)
            {
                if (context.Response.HasStarted)
                {
                    //logger
                    throw;
                }

                StringBuilder exceptionMessageBuilder = new StringBuilder();

                foreach (var innerException in exception.GetInnerExceptions())
                {
                    exceptionMessageBuilder.Append(innerException.Message);
                }

                #region TODO log


                //TODO: Логгирование
                //string loggermsg = exceptionMessageBuilder.ToString();

                //if (ex is ValidationException exv)
                //{
                //    loggermsg = exv.LoggerMsg;
                //}                
                //var tokenUser = JsonConvert.DeserializeObject<User>(context.User.FindFirst("User").Value);                
                //var dbContext = context.RequestServices.GetService(typeof(PPDBContext)) as PPDBContext;
                //var record = new LogRecord()
                //{
                //    Command = context.Request.Path.Value,
                //    DateTime = DateTime.Now,
                //    UserId = tokenUser.Id,
                //    Result = ex is ValidationException ? LogRecord.CommandResult.ValidationError : LogRecord.CommandResult.Exception,
                //    Notes = loggermsg
                //};

                ////специально синхронно
                //dbContext.LogRecords.Add(record);
                //dbContext.SaveChanges();

                #endregion

                int code = StatusCodes.Status500InternalServerError;

                if (exception is UnauthorizedAccessException)
                    code = StatusCodes.Status401Unauthorized;

                if (exception is PPValidationException)
                    code = StatusCodes.Status400BadRequest;

                context.Response.Clear();
                context.Response.StatusCode = code;
                context.Response.ContentType = @"text/plain";

                await context.Response.WriteAsync(exceptionMessageBuilder.ToString());

                return;
            }

        }       
    }

    public static class ExceptionMiddlewareExtensions
    {
        public static IApplicationBuilder UseExceptionMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionMiddleware>();
        }
    }    
}
