﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using DixiOmnes.PreflopPrism.Web.Filters;
using DixiOmnes.PreflopPrism.Web.Repositories;
using DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services;
using DixiOmnes.PreflopPrism.Web.Middleware;
using FluentValidation.AspNetCore;
using DixiOmnes.PreflopPrism.Web.Utils;
using DixiOmnes.PreflopPrism.Web.Models.Entities;
using DixiOmnes.PreflopPrism.Web.Services;

namespace PPWebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
#if DEBUG
            string con = Configuration.GetConnectionString(Environment.MachineName);
#else            
            string con = Configuration.GetConnectionString("ServerDB"); 
#endif

            services.AddDbContext<PPDBContext>(options => options.UseSqlServer(con));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        ValidIssuer = AuthOptions.ISSUER,
                        ValidateAudience = false,
                        ValidAudience = AuthOptions.AUDIENCE,
                        ValidateLifetime = false,
                        IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                        ValidateIssuerSigningKey = true
                    };
                });

            services.AddMvc(options =>
           {
               options.Filters.Add(typeof(ModelStateValidationFilterAttribute));
           }).AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.AddTransient(typeof(IUsersRepository), typeof(UsersRepository));
            services.AddTransient(typeof(IHardwareHashRepository), typeof(HardwareHashRepository));
            services.AddTransient(typeof(IProfilesRepository), typeof(ProfilesRepository));
            services.AddTransient(typeof(IRatingsRepository), typeof(RatingsRepository));


            services.AddTransient(typeof(IAccountService), typeof(AccountService));
            services.AddTransient(typeof(IUserService), typeof(UserService));
            services.AddTransient(typeof(IProfileService), typeof(ProfileService));
            services.AddTransient(typeof(IRatingService), typeof(RatingService));
            services.AddTransient(typeof(IStatsService), typeof(StatsService));

#if DEBUG
            services.AddTransient(typeof(IEmailSender), typeof(FakeEmailSender));
#else
            services.AddTransient(typeof(IEmailSender), typeof(EmailSender));
#endif
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseExceptionMiddleware();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
