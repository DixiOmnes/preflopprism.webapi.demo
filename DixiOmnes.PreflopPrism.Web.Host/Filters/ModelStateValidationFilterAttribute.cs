﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Filters
{
    public class ModelStateValidationFilterAttribute : Attribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
            
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = context.ModelState.Values.SelectMany(v => v.Errors);

                var errorBuilder = new StringBuilder();

                foreach (var err in allErrors)
                {
                    errorBuilder.AppendLine($"{err.ErrorMessage}");
                }

                context.Result = new BadRequestObjectResult(errorBuilder.ToString());
            }
        }
    }
}
