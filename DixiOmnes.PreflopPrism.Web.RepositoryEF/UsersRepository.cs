﻿using System.Threading.Tasks;
using DixiOmnes.PreflopPrism.Web.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace DixiOmnes.PreflopPrism.Web.Repositories
{
    public class UsersRepository : Repository, IUsersRepository
    {
        public UsersRepository(PPDBContext context) : base(context)
        {
        }

        public async Task<User> GetUserAsync(int userID)
        {
            return await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userID);
        }

        public async Task InsertUserAsync(User user)
        {
            await _dbContext.Users.AddAsync(user);
            await SaveChangesAsync();
        }

        public async Task<bool> EmailExistsAsync(string email)
        {
            return await _dbContext.Users.FirstOrDefaultAsync(x => x.EMail == email) != null;
        }

        public async Task<bool> UserNameExistsAsync(string name)
        {
            return await _dbContext.Users.FirstOrDefaultAsync(x => x.Name == name) != null;
        }          
    }
}
