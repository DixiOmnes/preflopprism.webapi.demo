﻿using DixiOmnes.PreflopPrism.Web.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Repositories
{
    public class Repository : IRepository
    {
        protected readonly PPDBContext _dbContext;        

        public Repository(PPDBContext context)
        {
            _dbContext = context;            
        }        

        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

        //public async Task WriteSuccessLogAsync(string path, int userID, string notes = "")
        //{
        //    await _dbContext.AddLogRecordAsync(path, userID, LogRecord.CommandResult.Success, notes);
        //}
    }
}
