﻿using DixiOmnes.PreflopPrism.Web.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DixiOmnes.PreflopPrism.Web.Migrations
{
    [DbContext(typeof(PPDBContext))]
    [Migration("20171214130527_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.0-rtm-26452")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DixiOmnes.PreflopPrism.Web.Models.Entities.Comment", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("comment_id");

                    b.Property<string>("Body")
                        .HasColumnName("comment")
                        .HasMaxLength(1000);

                    b.Property<DateTime>("DateTime")
                        .HasColumnName("date");

                    b.Property<int>("DisLikeCount")
                        .HasColumnName("dislike_count");

                    b.Property<int>("LikeCount")
                        .HasColumnName("like_count");

                    b.Property<int>("ProfileID")
                        .HasColumnName("profile_id");

                    b.Property<int>("UserID")
                        .HasColumnName("user_id");

                    b.HasKey("ID");

                    b.HasIndex("ProfileID", "DateTime");

                    b.HasIndex("UserID", "DateTime");

                    b.ToTable("comments");
                });

            modelBuilder.Entity("DixiOmnes.PreflopPrism.Web.Models.Entities.LogRecord", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<string>("Command")
                        .HasColumnName("command");

                    b.Property<DateTime>("DateTime")
                        .HasColumnName("datetime");

                    b.Property<string>("Notes")
                        .HasColumnName("notes")
                        .HasMaxLength(255);

                    b.Property<int>("Result")
                        .HasColumnName("command_result");

                    b.Property<int>("UserId")
                        .HasColumnName("user_id");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("logs");
                });

            modelBuilder.Entity("DixiOmnes.PreflopPrism.Web.Models.Entities.Profile", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("profile_id");

                    b.Property<string>("ActionLayers")
                        .IsRequired()
                        .HasColumnName("action_layers");

                    b.Property<double>("AverageRating")
                        .HasColumnName("average_rating");

                    b.Property<string>("Description")
                        .HasColumnName("description")
                        .HasMaxLength(1000);

                    b.Property<string>("GameType")
                        .IsRequired()
                        .HasColumnName("game_type")
                        .HasMaxLength(10);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("name")
                        .HasMaxLength(80);

                    b.Property<int>("StackSize")
                        .HasColumnName("stack_size");

                    b.Property<int>("Status")
                        .HasColumnName("status");

                    b.Property<int>("TableMax")
                        .HasColumnName("table_max");

                    b.Property<int>("UserId")
                        .HasColumnName("user_id");

                    b.Property<string>("Version")
                        .HasColumnName("version")
                        .HasMaxLength(20);

                    b.HasKey("ID");

                    b.HasIndex("UserId");

                    b.ToTable("profiles");
                });

            modelBuilder.Entity("DixiOmnes.PreflopPrism.Web.Models.Entities.Rating", b =>
                {
                    b.Property<int>("UserID")
                        .HasColumnName("user_id");

                    b.Property<int>("ProfileID")
                        .HasColumnName("profile_id");

                    b.Property<DateTime>("DateTime")
                        .HasColumnName("date");

                    b.Property<int>("Value")
                        .HasColumnName("rating_value");

                    b.HasKey("UserID", "ProfileID");

                    b.HasAlternateKey("ProfileID", "UserID");

                    b.ToTable("ratings");
                });

            modelBuilder.Entity("DixiOmnes.PreflopPrism.Web.Models.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("user_id");

                    b.Property<string>("EMail")
                        .HasColumnName("e_mail")
                        .HasMaxLength(100);

                    b.Property<string>("HardwareHash")
                        .IsRequired()
                        .HasColumnName("hardware_hash")
                        .HasMaxLength(50);

                    b.Property<string>("Name")
                        .HasColumnName("name")
                        .HasMaxLength(50);

                    b.Property<int>("Status")
                        .HasColumnName("status");

                    b.HasKey("Id");

                    b.HasAlternateKey("HardwareHash");

                    b.ToTable("users");
                });

            modelBuilder.Entity("DixiOmnes.PreflopPrism.Web.Models.Entities.Comment", b =>
                {
                    b.HasOne("DixiOmnes.PreflopPrism.Web.Models.Entities.Profile", "Profile")
                        .WithMany()
                        .HasForeignKey("ProfileID")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DixiOmnes.PreflopPrism.Web.Models.Entities.User", "User")
                        .WithMany()
                        .HasForeignKey("UserID")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("DixiOmnes.PreflopPrism.Web.Models.Entities.LogRecord", b =>
                {
                    b.HasOne("DixiOmnes.PreflopPrism.Web.Models.Entities.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("DixiOmnes.PreflopPrism.Web.Models.Entities.Profile", b =>
                {
                    b.HasOne("DixiOmnes.PreflopPrism.Web.Models.Entities.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("DixiOmnes.PreflopPrism.Web.Models.Entities.Rating", b =>
                {
                    b.HasOne("DixiOmnes.PreflopPrism.Web.Models.Entities.Profile", "Profile")
                        .WithMany()
                        .HasForeignKey("ProfileID")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("DixiOmnes.PreflopPrism.Web.Models.Entities.User", "User")
                        .WithMany()
                        .HasForeignKey("UserID")
                        .OnDelete(DeleteBehavior.Restrict);
                });
#pragma warning restore 612, 618
        }
    }
}
