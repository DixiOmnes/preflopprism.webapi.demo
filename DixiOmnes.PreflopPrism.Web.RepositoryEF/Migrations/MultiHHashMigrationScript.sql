ALTER TABLE [users] DROP CONSTRAINT [AK_users_hardware_hash];

GO

CREATE TABLE [hhashes] (
    [hhash_id] int NOT NULL IDENTITY,
    [user_id] int NOT NULL,
    [hhash_value] nvarchar(50) NOT NULL,
    CONSTRAINT [PK_hhashes] PRIMARY KEY ([hhash_id]),
    CONSTRAINT [AK_hhashes_hhash_value] UNIQUE ([hhash_value]),
    CONSTRAINT [FK_hhashes_users_user_id] FOREIGN KEY ([user_id]) REFERENCES [users] ([user_id]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_hhashes_hhash_value] ON [hhashes] ([hhash_value]);

GO

  INSERT INTO hhashes
        ( 
            [user_id],
            [hhash_value]
        )
        SELECT 
            [user_id],
            [hardware_hash]
        FROM [dbo].[users]		

DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'users') AND [c].[name] = N'hardware_hash');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [users] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [users] DROP COLUMN [hardware_hash];

GO



INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180402124613_Multi_HHash', N'2.0.2-rtm-10011');

GO