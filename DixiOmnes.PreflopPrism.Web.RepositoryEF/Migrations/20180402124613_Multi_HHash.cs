﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DixiOmnes.PreflopPrism.Web.Migrations
{
    public partial class Multi_HHash : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_users_hardware_hash",
                table: "users");

            migrationBuilder.DropColumn(
                name: "hardware_hash",
                table: "users");

            migrationBuilder.CreateTable(
                name: "hhashes",
                columns: table => new
                {
                    hhash_id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    user_id = table.Column<int>(nullable: false),
                    hhash_value = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_hhashes", x => x.hhash_id);
                    table.UniqueConstraint("AK_hhashes_hhash_value", x => x.hhash_value);
                    table.ForeignKey(
                        name: "FK_hhashes_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_hhashes_user_id",
                table: "hhashes",
                column: "user_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "hhashes");

            migrationBuilder.AddColumn<string>(
                name: "hardware_hash",
                table: "users",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_users_hardware_hash",
                table: "users",
                column: "hardware_hash");
        }
    }
}
