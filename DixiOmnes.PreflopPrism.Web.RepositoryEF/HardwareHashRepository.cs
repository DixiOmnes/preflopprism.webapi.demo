﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DixiOmnes.PreflopPrism.Web.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace DixiOmnes.PreflopPrism.Web.Repositories
{
    public class HardwareHashRepository : Repository, IHardwareHashRepository
    {
        public HardwareHashRepository(PPDBContext context) : base(context)
        {
        }

        public async Task<HardwareHash> GetHardwareHashAsync(string hhashString)
        {
            return await _dbContext.HardwareHashes.Include(p => p.User).FirstOrDefaultAsync(x => x.Value == hhashString);
        }

        public async Task InserthardwareHashAsync(HardwareHash hhash)
        {
            _dbContext.HardwareHashes.Add(hhash);

            await _dbContext.SaveChangesAsync();
        }
    }
}
