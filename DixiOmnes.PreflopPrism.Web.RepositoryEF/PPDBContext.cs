﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using System.Threading.Tasks;
using DixiOmnes.PreflopPrism.Web.Models.Common;

namespace DixiOmnes.PreflopPrism.Web.Models.Entities
{
    public class PPDBContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<LogRecord> LogRecords { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<HardwareHash> HardwareHashes { get; set; }

        public PPDBContext(DbContextOptions<PPDBContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);            

            builder.Entity<Rating>().HasKey(table => new { table.UserID, table.ProfileID });

            builder.Entity<Comment>().HasIndex(table => new { table.ProfileID, table.DateTime });
            builder.Entity<Comment>().HasIndex(table => new { table.UserID, table.DateTime });

            builder.Entity<HardwareHash>().HasAlternateKey(u => u.Value);

            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }        

        public async Task AddLogRecordAsync(string cmd, int userID, LogRecord.CommandResult result, string notes = "")
        {
            var record = new LogRecord()
            {
                Command = cmd,
                DateTime = DateTime.Now,
                UserId = userID,
                Result = result,
                Notes = notes
            };

            try
            {
                await LogRecords.AddAsync(record);
                await SaveChangesAsync();
            }
            catch (Exception) { }
        }
    }
}
