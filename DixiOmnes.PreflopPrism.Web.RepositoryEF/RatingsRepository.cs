﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DixiOmnes.PreflopPrism.Web.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace DixiOmnes.PreflopPrism.Web.Repositories
{
    public class RatingsRepository : Repository, IRatingsRepository
    {
        public RatingsRepository(PPDBContext context) : base(context)
        {
        }

        public void RemoveRating(Rating rating)
        {
            _dbContext.Ratings.Remove(rating);
        }

        public async Task<Rating> GetRatingAsync(int profileID, int userID)
        {
            return await _dbContext.Ratings.Include(p => p.Profile).Include(p => p.User).FirstOrDefaultAsync(x => x.ProfileID == profileID && x.UserID == userID);            
        }

        public async Task InsertRatingAsync(Rating rating)
        {
            await _dbContext.AddAsync(rating);
        }

        public async Task<int> GetTotalVotesForProfile(int profileID)
        {
            return await _dbContext.Ratings.CountAsync(p => p.ProfileID == profileID);
        }

        public async Task<double> GetAverageVotesForProfile(int profileID)
        {
            return await _dbContext.Ratings.Where(p => p.ProfileID == profileID).AverageAsync(r => r.Value);
        }
    }
}
