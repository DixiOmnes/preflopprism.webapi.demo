﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DixiOmnes.PreflopPrism.Web.Models.Common;
using DixiOmnes.PreflopPrism.Web.Models.Entities;
using DixiOmnes.PreflopPrism.Web.Models.Http;
using Microsoft.EntityFrameworkCore;

namespace DixiOmnes.PreflopPrism.Web.Repositories
{
    public class ProfilesRepository : Repository, IProfilesRepository
    {
        public ProfilesRepository(PPDBContext context) : base(context)
        {
        }

        public async Task<IEnumerable<ProfileViewModel>> GetAllProfilesListForUser(int userID)
        {
            var result = await _dbContext.Profiles
                .Where(p => p.Status == ProfileStatuses.Published || (p.UserId == userID && p.Status != ProfileStatuses.Hidden))
                .Select(p => new ProfileViewModel
                {
                    ID = p.ID,
                    Name = p.Name,
                    StackSize = p.StackSize,
                    Owner = p.User.Name,
                    GameType = p.GameType,
                    Description = p.Description,
                    Status = p.Status,
                    TableMax = p.TableMax,
                    Version = p.Version,
                    Rating = new RatingViewModel() { AverageRating = p.AverageRating, ProfileID = p.ID }
                }).ToListAsync();

            return result;
        }

        public async Task<IEnumerable<Profile>> GetPrivateProfilesForUser(int userID)
        {
            return await _dbContext.Profiles.Where(p => p.Status == ProfileStatuses.Private && p.User.Id == userID).ToArrayAsync();
        }

        public async Task<Profile> GetProfileAsync(int profileID)
        {
            return await _dbContext.Profiles.Include(p => p.User).FirstOrDefaultAsync(x => x.ID == profileID);
        }

        public async Task InsertProfileAsync(Profile profile)
        {
            await _dbContext.Profiles.AddAsync(profile);            
        }
    }
}
