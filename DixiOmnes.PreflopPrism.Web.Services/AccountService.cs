﻿using DixiOmnes.PreflopPrism.Web.Models.Common;
using DixiOmnes.PreflopPrism.Web.Models.Entities;
using DixiOmnes.PreflopPrism.Web.Repositories;
using DixiOmnes.PreflopPrism.Web.Utils;
using DixiOmnes.PreflopPrism.Web.Models.Exchange;
using DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace DixiOmnes.PreflopPrism.Web.Services
{
    public class AccountService : IAccountService
    {
        IHardwareHashRepository _hhashRepository;
        IUsersRepository _usersRepository;

        public AccountService(IHardwareHashRepository hhashrepository, IUsersRepository usersRepository)
        {
            _hhashRepository = hhashrepository;
            _usersRepository = usersRepository;
        }

        public async Task<TokenViewModel> CreateTokenFromHHashAsync(string hhash)
        {
            var dbHHash = await _hhashRepository.GetHardwareHashAsync(hhash);

            if (dbHHash == null)
            {
                using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var newUser = new User()
                    {
                        Status = UserStatuses.NotRegistered
                    };

                    await _usersRepository.InsertUserAsync(newUser);

                    dbHHash = new HardwareHash()
                    {
                        User = newUser,
                        Value = hhash
                    };

                    await _hhashRepository.InserthardwareHashAsync(dbHHash);

                    transaction.Complete();
                }
            }

            //по хорошему нужно добавлять функционал хранения и проверки выданных токенов, но пока так
            var user = dbHHash.User;

            return CreateToken(user);
        }        

        public TokenViewModel CreateToken(User dbUser)
        {
            var encodedJwt = GenerateTokenForUser(dbUser);

            return new TokenViewModel()
            {
                access_token = encodedJwt,
                lifetime = AuthOptions.LIFETIME
            };
        }

        #region TokenHelpers        

        private string GenerateTokenForUser(User user)
        {
            var identity = GenerateIdentityForUser(user);

            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }

        private static ClaimsIdentity GenerateIdentityForUser(User user)
        {
            string userStr = JsonConvert.SerializeObject(user);

            var claims = new List<Claim>
                {
                    new Claim("Status", ((int)user.Status).ToString()),
                    new Claim("ID", user.Id.ToString()),
                    new Claim("User", userStr),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Status.ToString())
                };

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            return claimsIdentity;
        }

        #endregion
    }
}
