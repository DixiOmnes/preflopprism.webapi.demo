﻿using DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace DixiOmnes.PreflopPrism.Web.Services
{
    public class EmailSender : IEmailSender
    {
        public void Send(string userName, string password, string host, MailMessage msg)
        {
            using (SmtpClient sc = new SmtpClient
            {
                Host = host,
                Port = 587,
                Credentials = new System.Net.NetworkCredential(userName, password),
                EnableSsl = true
            })
            {
                sc.Send(msg);
            }
        }
    }

    public class FakeEmailSender : IEmailSender
    {
        public void Send(string userName, string password, string host, MailMessage msg)
        {
            
        }
    }
}
