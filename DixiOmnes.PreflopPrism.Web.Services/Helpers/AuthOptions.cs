﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace DixiOmnes.PreflopPrism.Web.Utils
{
    public class AuthOptions
    {
        public const string ISSUER = "PPWebApiServer";
        public const string AUDIENCE = "PPUsers"; 
        const string KEY = "mysupersecret__secretkey!1234"; 
        public const int LIFETIME = 360; 

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
