﻿using DixiOmnes.PreflopPrism.Web.Models.Common;
using DixiOmnes.PreflopPrism.Web.Models.Entities;
using DixiOmnes.PreflopPrism.Web.Models.Http;
using DixiOmnes.PreflopPrism.Web.Repositories;
using DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DixiOmnes.PreflopPrism.Web.Models.Common;

namespace DixiOmnes.PreflopPrism.Web.Services
{
    public class ProfileService : IProfileService
    {
        IProfilesRepository _profilesRepository;
        IRatingsRepository _ratingsRepository;

        public ProfileService(IProfilesRepository profilesRepository, IRatingsRepository ratingsRepository)
        {
            _profilesRepository = profilesRepository;
            _ratingsRepository = ratingsRepository;
        }

        public async Task ChangeProfileStatusAsync(User user, ChangeProfileStatusViewModel requestProfile)
        {
            var dbProfile = await _profilesRepository.GetProfileAsync(requestProfile.ID);

            if (dbProfile == null)
                throw new PPValidationException($"Profile with id = {requestProfile.ID} does not exists");

            if (dbProfile.UserId != user.Id)
                throw new PPValidationException($"You can change only own profile statuses");

            if (requestProfile.Status == ProfileStatuses.Private)
            {
                var privateProfiles = await _profilesRepository.GetPrivateProfilesForUser(user.Id);

                if (privateProfiles.Count() > 0)
                    throw new PPValidationException($"Beta-tester can have only 1 private profile");
            }

            dbProfile.Status = requestProfile.Status;

            await _profilesRepository.SaveChangesAsync();
        }

        public async Task<ProfileViewModel> GetProfileAsync(User user, int profileID)
        {
            var dbProfile = await _profilesRepository.GetProfileAsync(profileID);

            if (dbProfile == null)
                throw new PPValidationException("Profile not found on server");

            if (user.Id != dbProfile.UserId && dbProfile.Status != ProfileStatuses.Published)
                throw new PPValidationException("Profile is not published");

            ProfileViewModel httpContextProfile = new ProfileViewModel
            {
                ID = dbProfile.ID,
                Name = dbProfile.Name,
                GameType = dbProfile.GameType,
                Description = dbProfile.Description,
                ActionLayers = dbProfile.ActionLayers,
                StackSize = dbProfile.StackSize,
                TableMax = dbProfile.TableMax,
                Version = dbProfile.Version,
                Status = dbProfile.Status,
                Owner = dbProfile.User.Name
            };

            var userRating = await _ratingsRepository.GetRatingAsync(dbProfile.ID, user.Id);

            httpContextProfile.Rating = new RatingViewModel()
            {
                AverageRating = dbProfile.AverageRating,
                ProfileID = dbProfile.ID,
                VoteTotal = await _ratingsRepository.GetTotalVotesForProfile(dbProfile.ID),
                UserRating = userRating != null ? userRating.Value : 0
            };

            return httpContextProfile;
        }

        public async Task<IEnumerable<ProfileViewModel>> GetProfilesListAsync(User user)
        {
            //TODO убрать из репозитория маппинг в HttpProfile
            var httpProfiles = await _profilesRepository.GetAllProfilesListForUser(user.Id);

            return httpProfiles;
        }

        public async Task<int> UpdateProfileAsync(User user, ProfileViewModel httpContextProfile)
        {
            Profile dbProfile;

            if (httpContextProfile.ID.HasValue)
            {
                dbProfile = await _profilesRepository.GetProfileAsync(httpContextProfile.ID.Value);

                if (dbProfile == null)
                    throw new PPValidationException($"Cant update! Profile with id = {httpContextProfile.ID.Value} does not exist");

                if (dbProfile.UserId != user.Id)
                    throw new PPValidationException($"You can update only your profiles");
            }
            else
            {
                dbProfile = new Profile
                {
                    Status = ProfileStatuses.Published
                };

                await _profilesRepository.InsertProfileAsync(dbProfile);
            }


            dbProfile.Name = httpContextProfile.Name;
            dbProfile.Description = httpContextProfile.Description;
            dbProfile.GameType = httpContextProfile.GameType;
            dbProfile.StackSize = httpContextProfile.StackSize;
            dbProfile.TableMax = httpContextProfile.TableMax;
            dbProfile.ActionLayers = httpContextProfile.ActionLayers;
            dbProfile.Version = httpContextProfile.Version;
            dbProfile.UserId = user.Id;

            await _profilesRepository.SaveChangesAsync();

            return dbProfile.ID;
        }
    }
}
