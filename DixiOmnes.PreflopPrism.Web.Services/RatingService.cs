﻿using DixiOmnes.PreflopPrism.Web.Models.Common;
using DixiOmnes.PreflopPrism.Web.Models.Entities;
using DixiOmnes.PreflopPrism.Web.Models.Http;
using DixiOmnes.PreflopPrism.Web.Repositories;
using DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services;
using System;
using System.Threading.Tasks;
using System.Transactions;

namespace DixiOmnes.PreflopPrism.Web.Services
{
    public class RatingService : IRatingService
    {
        IProfilesRepository _profilesRepository;
        IRatingsRepository _ratingsRepository;


        public RatingService(IProfilesRepository profilesRepository, IRatingsRepository ratingsRepository)
        {
            _profilesRepository = profilesRepository;
            _ratingsRepository = ratingsRepository;
        }

        public async Task<RatingViewModel> ChangeProfileRating(User user, RatingViewModel httpRating)
        {
            var profile = await _profilesRepository.GetProfileAsync(httpRating.ProfileID);

            if (profile == null)
                throw new PPValidationException($"Profile with id = {httpRating.ProfileID} does not exists");

            if (profile.UserId == user.Id)
                throw new PPValidationException("You cannot rate own profiles");

            if (profile.Status != ProfileStatuses.Published)
                throw new PPValidationException("You can rate only published profiles");

            if (httpRating.UserRating < 0 || httpRating.UserRating > 5)
                throw new PPValidationException($"{httpRating.UserRating} is incorrect rating value");

            var rating = await _ratingsRepository.GetRatingAsync(profile.ID, user.Id);

            using (var transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                //удаление рейтинга. Возможно вообще не нужно
                if (httpRating.UserRating == 0)
                {
                    if (rating != null)
                        _ratingsRepository.RemoveRating(rating);
                }

                if (httpRating.UserRating != 0)
                {
                    if (rating == null)
                    {
                        rating = new Rating()
                        {
                            ProfileID = profile.ID,
                            UserID = user.Id
                        };

                        await _ratingsRepository.InsertRatingAsync(rating);
                    }

                    rating.DateTime = DateTime.Now;
                    rating.Value = httpRating.UserRating;
                }


                await _ratingsRepository.SaveChangesAsync();

                profile.AverageRating = await _ratingsRepository.GetAverageVotesForProfile(profile.ID);

                await _profilesRepository.SaveChangesAsync();

                transaction.Complete();
            }

            var userRating = await _ratingsRepository.GetRatingAsync(profile.ID, user.Id);

            RatingViewModel result = null;

            if (userRating != null)
                result = new RatingViewModel()
                {
                    ProfileID = profile.ID,
                    AverageRating = profile.AverageRating,
                    VoteTotal = await _ratingsRepository.GetTotalVotesForProfile(profile.ID),
                    UserRating = userRating.Value
                };

            return result;
        }
    }
}
