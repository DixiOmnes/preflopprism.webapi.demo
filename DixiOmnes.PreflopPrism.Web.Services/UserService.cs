﻿using DixiOmnes.PreflopPrism.Web.Models.Common;
using DixiOmnes.PreflopPrism.Web.Models.Entities;
using DixiOmnes.PreflopPrism.Web.Models.Http;
using DixiOmnes.PreflopPrism.Web.Repositories;
using DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System;
using System.IO;
using DixiOmnes.PreflopPrism.Web.Models.Common;

namespace DixiOmnes.PreflopPrism.Web.Services
{
    public class UserService : IUserService
    {
        IUsersRepository _usersRepository;
        IEmailSender _EmailSender;

        public UserService(IUsersRepository usersRepository, IEmailSender EmailSender)
        {
            _usersRepository = usersRepository;
            _EmailSender = EmailSender;
        }

        public async Task<UserViewModel> SendEmailVerificationLetterAsync(User userJWT, UserViewModel userHTTPContext)
        {
            #region Валидация данных                        

            var errorBuilder = new StringBuilder();

            if (await _usersRepository.EmailExistsAsync(userHTTPContext.EMail))
            {
                errorBuilder.AppendLine($"E-mail \"{userHTTPContext.EMail}\" already exists. ");
            }

            if (await _usersRepository.UserNameExistsAsync(userHTTPContext.UserName))
            {
                errorBuilder.AppendLine($"Username \"{userHTTPContext.UserName}\" already exists. ");
            }

            if (!string.IsNullOrEmpty(errorBuilder.ToString()))
                throw new PPValidationException(errorBuilder.ToString());

            #endregion

            var dbUser = await GetUserAsync(userJWT.Id);

            dbUser.Status = UserStatuses.EmailValidation;
            dbUser.Name = userHTTPContext.UserName;
            dbUser.EMail = userHTTPContext.EMail;

            await SendEmailVerificationCode(dbUser);

            await _usersRepository.SaveChangesAsync();

            return MapDBUserToHttpUser(dbUser);
        }

        public async Task<UserViewModel> ValidateEmailAsync(User userJWT, string emailCodeFromHttpContext)
        {
            var dbUser = await GetUserAsync(userJWT.Id);

            if (emailCodeFromHttpContext == GetEmailVerificationCode(dbUser))
            {
                dbUser.Status = UserStatuses.Registered;

                await _usersRepository.SaveChangesAsync();

                return MapDBUserToHttpUser(dbUser);
            }
            else
            {
                throw new PPValidationException($"{emailCodeFromHttpContext} - Неверный код валидации");
            }
        }

        public async Task<UserViewModel> CancelUserStatusAsync(User userJWT)
        {
            var dbUser = await GetUserAsync(userJWT.Id);

            dbUser.Status = UserStatuses.NotRegistered;
            dbUser.EMail = null;
            dbUser.Name = null;

            await _usersRepository.SaveChangesAsync();

            return MapDBUserToHttpUser(dbUser);
        }

        public async Task<UserViewModel> GetUserAsync(User userJWT)
        {
            var dbUser = await GetUserAsync(userJWT.Id);

            return MapDBUserToHttpUser(dbUser); ;
        }

        private async Task<User> GetUserAsync(int id)
        {
            var user = await _usersRepository.GetUserAsync(id);

            if (user == null)
                throw new PPValidationException($"Не найден пользователь с id = {id}");

            return user;
        }



        private UserViewModel MapDBUserToHttpUser(User dbUser)
        {
            return new UserViewModel()
            {
                UserName = dbUser.Name,
                EMail = dbUser.EMail,
                Status = dbUser.Status
            };
        }

        #region EmailHelpers

        private async Task SendEmailVerificationCode(User user)
        {
            MailMessage msg = new MailMessage
            {
                From = new MailAddress("preflopprism@gmail.com", "Preflop Prism"),
                Subject = "E-mail verification",
                Body = GetEmailVerificationCode(user)
            };

            msg.To.Clear();
            msg.To.Add(user.EMail);

            //TODO:переделать на конфиг
            _EmailSender.Send("preflopprism@gmail.com", await LoadPasswordFromFile("email_password.txt"), "smtp.gmail.com", msg);
        }

        private async Task<string> LoadPasswordFromFile(string path)
        {
            using (StreamReader sr = new StreamReader(path))
            {
                return await sr.ReadToEndAsync();
            }
        }

        private string GetEmailVerificationCode(User user)
        {
#if DEBUG
            return "1111";
#else
            return MD5HashHelper.GenerateEmailValidationHash(user.EMail);
#endif
        }

        #endregion
    }
}
