﻿using DixiOmnes.PreflopPrism.Web.Models.Common;
using DixiOmnes.PreflopPrism.Web.Models.Entities;
using DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Services
{
    public class StatsService : IStatsService
    {
        PPDBContext _dbContext;        

        public StatsService(PPDBContext context)
        {
            _dbContext = context;            
        }

        public async Task<string> GetStatsInHTMLForm(int code, int hours, string webRootPath)
        {
            if (code != 5834243)
                throw new UnauthorizedAccessException();

            if (hours == 0)
                hours = 12;

            var totalusers = await _dbContext.Users.CountAsync();
            var totalprofiles = await _dbContext.Profiles.CountAsync();
            var totalprofilespub = await _dbContext.Profiles.Where(p => p.Status == ProfileStatuses.Published).CountAsync();
            var totalprofilesprivate = await _dbContext.Profiles.Where(p => p.Status == ProfileStatuses.Private).CountAsync();
            var totalprofilesdel = await _dbContext.Profiles.Where(p => p.Status == ProfileStatuses.Hidden).CountAsync();

            var dt = DateTime.Now.Subtract(TimeSpan.FromHours(hours));
            var usersActiveLastXhours = await _dbContext.LogRecords
                .Where(p => p.DateTime > dt && p.Command.Contains("users/get_user_status")).OrderBy(s => s.DateTime)
                .Select(x => new { x.UserId, x.User.Name, x.User.EMail, x.Notes, x.DateTime }).ToListAsync();
           
            var file = System.IO.Path.Combine(webRootPath, @"stats\stats.html");
            var htmlContent = await System.IO.File.ReadAllTextAsync(file);

            htmlContent = htmlContent.Replace("@users", totalusers.ToString());

            htmlContent = htmlContent.Replace("@totalprofiles", totalprofiles.ToString());
            htmlContent = htmlContent.Replace("@publishedprofiles", totalprofilespub.ToString());
            htmlContent = htmlContent.Replace("@privateprofiles", totalprofilesprivate.ToString());
            htmlContent = htmlContent.Replace("@deletedprofiles", totalprofilesdel.ToString());

            string tbody = "<tbody>";
            foreach (var userActivity in usersActiveLastXhours)
            {
                tbody += $"<tr><td>{userActivity.UserId}</td>";
                tbody += $"<td>{userActivity.Name}</td>";
                tbody += $"<td>{userActivity.EMail}</td>";
                tbody += $"<td>{userActivity.Notes}</td>";
                tbody += $"<td>{userActivity.DateTime.ToString("dd.MM HH:mm")}</td></tr>";
            }
            tbody += "</tbody>";

            htmlContent = htmlContent.Replace("@activitybody", tbody);

            return htmlContent;
        }
    }
}
