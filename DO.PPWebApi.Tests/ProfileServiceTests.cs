﻿using DixiOmnes.PreflopPrism.Web.Models.Common;
using DixiOmnes.PreflopPrism.Web.Models.Entities;
using DixiOmnes.PreflopPrism.Web.Models.Http;
using DixiOmnes.PreflopPrism.Web.Repositories;
using DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services;
using DixiOmnes.PreflopPrism.Web.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DixiOmnes.PreflopPrism.Web.Models.Common;

namespace DixiOmnes.PreflopPrism.Web.Tests
{
    [TestClass]
    public class ProfileServiceTests
    {
        Mock<IProfilesRepository> _profilesRepositoryMock = new Mock<IProfilesRepository>();
        Mock<IRatingsRepository> _ratingsRepositoryMock = new Mock<IRatingsRepository>();
        IProfileService _profileService;

        public ProfileServiceTests()
        {
            _profileService = new ProfileService(_profilesRepositoryMock.Object, _ratingsRepositoryMock.Object);
        }

        [TestMethod]
        public async Task ChangeProfileStatusAsyncTest1()
        {
            _profilesRepositoryMock.Setup(x => x.GetProfileAsync(It.IsAny<int>())).ReturnsAsync((Profile)null);

            var dbUser = new User()
            {
                Id = 1,
                EMail = "anymail",
                Name = "anyname",
                Status = UserStatuses.EmailValidation
            };

            var requestProfile = new ChangeProfileStatusViewModel()
            {
                ID = 1,
                Status = ProfileStatuses.Private
            };

            //throw new PPValidationException($"Profile with id = {requestProfile.ID} does not exists")
            await Assert.ThrowsExceptionAsync<PPValidationException>(() => _profileService.ChangeProfileStatusAsync(dbUser, requestProfile));
        }

        [TestMethod]
        public async Task ChangeProfileStatusAsyncTest2()
        {
            _profilesRepositoryMock.Setup(x => x.GetProfileAsync(It.IsAny<int>())).ReturnsAsync(new Profile()
            {
                ID = 1,
                UserId = 2
            }
            );

            var dbUser = new User()
            {
                Id = 1,
                EMail = "anymail",
                Name = "anyname",
                Status = UserStatuses.EmailValidation
            };

            var requestProfile = new ChangeProfileStatusViewModel()
            {
                ID = 1,
                Status = ProfileStatuses.Private
            };

            //throw new PPValidationException($"You can change only own profile statuses")
            await Assert.ThrowsExceptionAsync<PPValidationException>(() => _profileService.ChangeProfileStatusAsync(dbUser, requestProfile));
        }

        [TestMethod]
        public async Task ChangeProfileStatusAsyncTest3()
        {
            _profilesRepositoryMock.Setup(x => x.GetProfileAsync(It.IsAny<int>())).ReturnsAsync(new Profile()
            {
                ID = 1,
                UserId = 1
            }
            );

            _profilesRepositoryMock.Setup(x => x.GetPrivateProfilesForUser(It.IsAny<int>())).ReturnsAsync(new List<Profile>()
            {
                new Profile(),
                new Profile()
            }
            );

            var dbUser = new User()
            {
                Id = 1,
                EMail = "anymail",
                Name = "anyname",
                Status = UserStatuses.EmailValidation
            };

            var requestProfile = new ChangeProfileStatusViewModel()
            {
                ID = 1,
                Status = ProfileStatuses.Private
            };

            //throw new PPValidationException($"Beta-tester can have only 1 private profile")
            await Assert.ThrowsExceptionAsync<PPValidationException>(() => _profileService.ChangeProfileStatusAsync(dbUser, requestProfile));
        }
    }
}
