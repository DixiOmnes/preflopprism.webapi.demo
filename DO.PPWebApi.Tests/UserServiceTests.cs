using DixiOmnes.PreflopPrism.Web.Models.Common;
using DixiOmnes.PreflopPrism.Web.Models.Entities;
using DixiOmnes.PreflopPrism.Web.Repositories;
using DixiOmnes.PreflopPrism.Web.Models.Interfaces.Services;
using DixiOmnes.PreflopPrism.Web.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace DixiOmnes.PreflopPrism.Web.Tests
{
    [TestClass]
    public class UserServiceTests
    {
        Mock<IEmailSender> _emailSenderMock = new Mock<IEmailSender>();
        Mock<IUsersRepository> _userRepositoryMock = new Mock<IUsersRepository>();
        IUserService _userService;

        public UserServiceTests()
        {
            _userService = new UserService(_userRepositoryMock.Object, _emailSenderMock.Object);
        }

        [TestMethod]
        public async Task CancelUserStatusTest()
        {
            _userRepositoryMock.Setup(x => x.GetUserAsync(It.IsAny<int>())).Returns(Task.FromResult(new User()
            {
                Id = 1,
                EMail = "anymail",
                Name = "anyname",
                Status = UserStatuses.EmailValidation
            }));


            var dbUser = new User()
            {
                Id = 1,
                EMail = "anymail",
                Name = "anyname",
                Status = UserStatuses.EmailValidation
            };

            var result = await _userService.CancelUserStatusAsync(dbUser);

            Assert.AreEqual(result.Status, UserStatuses.NotRegistered);
            Assert.AreEqual(result.UserName, null);
            Assert.AreEqual(result.EMail, null);
        }

        [TestMethod]
        public async Task GetUserStatusTest()
        {
            _userRepositoryMock.Setup(x => x.GetUserAsync(It.IsAny<int>())).Returns(Task.FromResult(new User()
            {
                Id = 1,
                EMail = "mail1",
                Name = "name1",
                Status = UserStatuses.Registered
            }));


            var dbUser = new User()
            {
                Id = 1,
                EMail = null,
                Name = null,
                Status = UserStatuses.Unknown
            };

            var result = await _userService.GetUserAsync(dbUser);

            Assert.AreEqual(result.Status, UserStatuses.Registered);
            Assert.AreEqual(result.UserName, "name1");
            Assert.AreEqual(result.EMail, "mail1");
        }
    }
}
